package ru.mjs.model.client.exception;

import lombok.Data;
import ru.mjs.model.client.enums.ReturnCodeEnum;

/**
 * @author procki-u-a * * 
 */
@Data
public class BattlecityOnlineBaseException extends RuntimeException {
    private static final long serialVersionUID = 2047202391634642212L;
    
    private ReturnCodeEnum returnCodeEnum;

    public BattlecityOnlineBaseException(ReturnCodeEnum returnCodeEnum) {
        this.returnCodeEnum = returnCodeEnum;
    }
}
