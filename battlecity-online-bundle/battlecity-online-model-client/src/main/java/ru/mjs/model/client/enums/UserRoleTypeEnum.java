package ru.mjs.model.client.enums;

import java.util.HashMap;
import java.util.Map;

public enum UserRoleTypeEnum {
	USER("USER"),
	ADMIN("ADMIN"),
	UNKNOWN("UNKNOWN");
	
	private String name;
	
	private static Map<String, UserRoleTypeEnum> map = new HashMap<String, UserRoleTypeEnum>();
	
	static {
		for(UserRoleTypeEnum ur : UserRoleTypeEnum.values()) {
			map.put(ur.name, ur);
		}
	}
	
	private UserRoleTypeEnum(String name) {
		this.name = name;
	}

	public static UserRoleTypeEnum find(String name) {
		UserRoleTypeEnum codeEnum = map.get(name);
		return codeEnum == null ? UNKNOWN : codeEnum;
	}

    public String getName() {
        return name;
    }
}
