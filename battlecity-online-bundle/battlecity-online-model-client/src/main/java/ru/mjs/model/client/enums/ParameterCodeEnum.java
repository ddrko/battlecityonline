package ru.mjs.model.client.enums;

import java.util.HashMap;
import java.util.Map;

public enum ParameterCodeEnum {
	LOGIN("LOGIN"),
	PASSWORD("PASSWORD"),
	NICKNAME("NICKNAME"),
	RETURN_CODE("RETURN_CODE"),
	SERVER_VERSION("SERVER_VERSION"),
	COMMAND_TYPE("COMMAND_TYPE"),
	UNKNOWN("UNKNOWN");
	
	private String name;
	
	private static Map<String, ParameterCodeEnum> map = new HashMap<String, ParameterCodeEnum>();
	
	static {
		for(ParameterCodeEnum cd : ParameterCodeEnum.values()) {
			map.put(cd.name, cd);
		}
	}
	
	private ParameterCodeEnum(String name) {
		this.name = name;
	}

	public static ParameterCodeEnum find(String name) {
		ParameterCodeEnum codeEnum = map.get(name);
		return codeEnum == null ? UNKNOWN : codeEnum;
	}

    public String getName() {
        return name;
    }
}
