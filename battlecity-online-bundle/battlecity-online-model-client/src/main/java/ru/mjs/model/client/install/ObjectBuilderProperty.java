package ru.mjs.model.client.install;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;

/**
 * @author procki-u-a * * 
 */
@Data
public class ObjectBuilderProperty {
    private String className;
    private List<String> naturalKeys = new ArrayList<String>();
    private Map<String, String> params = new HashMap<String, String>();
}