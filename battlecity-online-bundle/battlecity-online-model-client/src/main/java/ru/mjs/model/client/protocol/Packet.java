package ru.mjs.model.client.protocol;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

/**
 * @author procki-u-a * * 
 */
@Data
public class Packet {
    private Boolean c2dictionary = Boolean.TRUE;
    private Map<String, String> data = new HashMap<String, String>();
}