package ru.mjs.model.client.enums;

import java.util.HashMap;
import java.util.Map;

public enum CacheCodeEnum {
	USER_ROLE_CACHE("USER_ROLE_CACHE"),
	USER_ACCOUNT_CACHE("USER_ACCOUNT_CACHE"),
	UNKNOWN("UNKNOWN");
	
	private String name;
	
	private static Map<String, CacheCodeEnum> map = new HashMap<String, CacheCodeEnum>();
	
	static {
		for(CacheCodeEnum cc : CacheCodeEnum.values()) {
			map.put(cc.name, cc);
		}
	}
	
	private CacheCodeEnum(String name) {
		this.name = name;
	}

	public static CacheCodeEnum find(String name) {
		CacheCodeEnum codeEnum = map.get(name);
		return codeEnum == null ? UNKNOWN : codeEnum;
	}

    public String getName() {
        return name;
    }
}
