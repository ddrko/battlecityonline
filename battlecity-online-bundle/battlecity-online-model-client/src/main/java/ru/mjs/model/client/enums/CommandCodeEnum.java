package ru.mjs.model.client.enums;

import java.util.HashMap;
import java.util.Map;

public enum CommandCodeEnum {
	CHECK_VERSION("CHECK_VERSION"),
	LOG_IN("LOG_IN"),
	REGISTRATION("REGISTRATION"),
	UNKNOWN("UNKNOWN");
	
	private String name;
	
	private static Map<String, CommandCodeEnum> map = new HashMap<String, CommandCodeEnum>();
	
	static {
		for(CommandCodeEnum cd : CommandCodeEnum.values()) {
			map.put(cd.name, cd);
		}
	}
	
	private CommandCodeEnum(String name) {
		this.name = name;
	}

	public static CommandCodeEnum find(String name) {
		CommandCodeEnum codeEnum = map.get(name);
		return codeEnum == null ? UNKNOWN : codeEnum;
	}

    public String getName() {
        return name;
    }
}
