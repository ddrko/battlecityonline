package ru.mjs.model.client.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * @author procki-u-a * * 
 */
public enum ReturnCodeEnum {
    /** OK */
    RC_0001("RC_0001"),
    /** INTERNAL ERROR */
    RC_0002("RC_0002"),
    /** BAD TCP PACKET */
    RC_0003("RC_0003"),
    /** USER IS NOT JOINED TO LOBBY */
    RC_0004("RC_0004"),
    /** SERVER IS FULL */
    RC_0005("RC_0005"),
    /** USER IS NOT AUTHORISED */
    RC_0006("RC_0006"),
    /** LOBBY NOT FOUND */
    RC_0007("RC_0007"),
    /** LOBBY IS FULL */
    RC_0008("RC_0008"),
    /** WRONG LOBBY PASSWORD */
    RC_0009("RC_0009"),
    /** COMMAND TYPE NOT FOUND */
    RC_0010("RC_0010"),
    /** BAD COMMAND CODE */
    RC_0011("RC_0011"),
    /** NICKNAME NOT FOUND */
    RC_0012("RC_0012"),
    /** LOGIN NOT FOUND */
    RC_0013("RC_0013"),
    /** PASSWORD NOT FOUND */
    RC_0014("RC_0014"),
    /** DB: USER ROLE NOT FOUND */
    RC_0015("RC_0015"),
    /** DB: USER FROM THIS LOGIN EXISTS */
    RC_0016("RC_0016"),
    /** DB: USER FROM THIS LOGIN AND PASSWORD NOT FOUND */
    RC_0017("RC_0017"),
    /** SERVER VERSION NOT FOUND */
    RC_0018("RC_0018"),
    /** INCORRECT SERVER VERSION */
    RC_0019("RC_0019"),
    
    /** UNKNOWN */
    RC_9999("RC_9999");
    
    private String name;
    private static Map<String, ReturnCodeEnum> map = new HashMap<String, ReturnCodeEnum>();
    
    static {
        for(ReturnCodeEnum rc : ReturnCodeEnum.values()) {
            map.put(rc.name, rc);
        }
    } 
    
    private ReturnCodeEnum(String name) {
        this.name = name;
    }
    
    public static ReturnCodeEnum find(String name) {
        ReturnCodeEnum codeEnum = map.get(name);
        return codeEnum == null ? RC_9999 : codeEnum;
    }

    public String getName() {
        return name;
    }
}
