package ru.mjs.install;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ru.mjs.install.util.GsonGeneratorUtil;
import ru.mjs.model.client.install.ObjectBuilderProperty;
import ru.mjs.model.database.UserAccountDO;
import ru.mjs.model.database.UserAccountRoleDO;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author procki-u-a * *
 */
public class GsonGeneratorUtilTest extends BaseTest {
    @Autowired
    private GsonGeneratorUtil gsonGeneratorUtil;

    @Test
    public void toJsonTest() {
        System.out.println("toJsonTest");

        List<UserAccountDO> users = generateTestUserAccauntEntity();
        String toGson = gsonGeneratorUtil.toJson(users);

        assertFalse(StringUtils.isBlank(toGson));
    }

    @Test
    public void fromJsonToObjectTest() {
        System.out.println("fromJsonToObjectTest");

        UserAccountDO user = generateTestUserAccauntEntity().get(0);
        String toGson = gsonGeneratorUtil.toJson(user);

        assertFalse(StringUtils.isBlank(toGson));

        UserAccountDO fromGson = gsonGeneratorUtil.fromJsonToObject(toGson, UserAccountDO.class);

        assertNotNull(fromGson);
    }

    @Test
    public void fromJsonToList() {
        System.out.println("fromJsonToList");

        List<UserAccountDO> users = generateTestUserAccauntEntity();
        String toGson = gsonGeneratorUtil.toJson(users);

        assertFalse(StringUtils.isBlank(toGson));

        List<UserAccountDO> fromGson = gsonGeneratorUtil.fromJsonToList(toGson, UserAccountDO.class);

        assertNotNull(fromGson);
    }

    @Test
    public void fromJsonToMap() {
        System.out.println("fromJsonToMap");

        Map<String, ObjectBuilderProperty> property = generateTestObjectBuilderProperty();
        String toGson = gsonGeneratorUtil.toJson(property);

        assertFalse(StringUtils.isBlank(toGson));

        Map<String, ObjectBuilderProperty> fromGson = gsonGeneratorUtil.fromJsonToMap(toGson,
                ObjectBuilderProperty.class);

        assertNotNull(fromGson);
    }

    private Map<String, ObjectBuilderProperty> generateTestObjectBuilderProperty() {
        Map<String, ObjectBuilderProperty> result = Maps.newHashMap();

        ObjectBuilderProperty property = new ObjectBuilderProperty();
        property.setClassName("ru.mjs.model.UserAccountDO");
        property.setNaturalKeys(Lists.newArrayList("login", "nickname"));

        result.put("UserAccountDO", property);
        return result;
    }

    private List<UserAccountDO> generateTestUserAccauntEntity() {
        UserAccountRoleDO userRole = new UserAccountRoleDO();
        userRole.setName("USER");
        UserAccountRoleDO adminRole = new UserAccountRoleDO();
        adminRole.setName("ADMIN");

        UserAccountDO adminUser = new UserAccountDO();
        adminUser.setDisabled(Boolean.FALSE);
        adminUser.setLogin("Frest1987@Yandex.ru");
        adminUser.setNickname("Frest1987");
        adminUser.setPassword("18011987");
        adminUser.setUserAccountRole(adminRole);
        adminUser.setRegistrationDate(new Date());

        UserAccountDO normalUser = new UserAccountDO();
        normalUser.setDisabled(Boolean.FALSE);
        normalUser.setLogin("NormalUser@Test.ru");
        normalUser.setNickname("NormalUser");
        normalUser.setPassword("12345678");
        normalUser.setUserAccountRole(userRole);
        normalUser.setRegistrationDate(new Date());

        List<UserAccountDO> result = Lists.newArrayList(adminUser, normalUser);
        return result;
    }
}