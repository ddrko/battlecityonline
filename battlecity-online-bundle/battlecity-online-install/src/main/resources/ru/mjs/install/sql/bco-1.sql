if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('dbo.UserAccount') and o.name = 'FK_USERACCO_FK_USERAC_USERACCO')
alter table dbo.UserAccount
   drop constraint FK_USERACCO_FK_USERAC_USERACCO

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.UserAccount')
            and   type = 'U')
   drop table dbo.UserAccount

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.UserAccountRole')
            and   type = 'U')
   drop table dbo.UserAccountRole

create table dbo.UserAccount (
   UserAccountID        int                  identity,
   UserAccountRoleID    int                  null,
   Login                varchar(50)          null,
   Password             varchar(50)          null,
   Nickname             varchar(50)          null,
   Disabled             tinyint              null,
   RegistrationDate     datetime             null,
   LastSignInDate       datetime             null,
   constraint PK_USERACCOUNT primary key (UserAccountID),
   constraint AK_AK_LOGIN_USERACCO unique (Login),
   constraint AK_AK_NICKNAME_USERACCO unique (Nickname)
)

create table dbo.UserAccountRole (
   UserAccountRoleID    int                  identity,
   Name                 varchar(50)          null,
   constraint PK_USERACCOUNTROLE primary key (UserAccountRoleID)
)

alter table dbo.UserAccount
   add constraint FK_USERACCO_FK_USERAC_USERACCO foreign key (UserAccountRoleID)
      references dbo.UserAccountRole (UserAccountRoleID)