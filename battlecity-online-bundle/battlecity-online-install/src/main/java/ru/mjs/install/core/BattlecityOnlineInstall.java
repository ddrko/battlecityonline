package ru.mjs.install.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import ru.mjs.install.processor.GsonObjectBuildProcessor;
import ru.mjs.install.processor.SqlObjectBuildProcessor;
import ru.mjs.install.util.GsonGeneratorUtil;
import ru.mjs.install.util.ResourceLoadUtil;
import ru.mjs.model.client.install.ObjectBuilderProperty;

/**
 * @author procki-u-a * *
 */
@Controller
public class BattlecityOnlineInstall {
    private static String BUILD_CONFIG_PATH = "/ru/mjs/install/config/install.config";
    private static String LST_CONFIG_PATH = "/ru/mjs/install/config/install.lst";
    
    private Map<String, ObjectBuilderProperty> builderConfig = new HashMap<String, ObjectBuilderProperty>();

    @Autowired
    private ResourceLoadUtil resourceLoadUtil;
    @Autowired
    private GsonGeneratorUtil gsonGeneratorUtil;
    @Autowired
    private GsonObjectBuildProcessor gsonObjectBuildProcessor;
    @Autowired
    private SqlObjectBuildProcessor sqlObjectBuildProcessor;
    
    
    
    @Transactional
    public void install() {
        initObjectBuilderConfig();
        
        Scanner scanner = new Scanner(getClass().getResourceAsStream(LST_CONFIG_PATH));
        
        while(scanner.hasNext()) {
            String scriptInfo = scanner.nextLine().trim();
            
            if(StringUtils.isBlank(scriptInfo) || scriptInfo.startsWith("#")) continue;
            
            System.out.println("Run script -> " + scriptInfo);
            
            if(StringUtils.equals(scriptInfo.split(" ")[0], "EXECUTE")) {
                installSql(scriptInfo.split(" ")[1]);
            } else {
                installGson(scriptInfo.split(" ")[1], scriptInfo.split(" ")[2]);
            }

            System.out.println("Installation completed!");
        }
    }
    
    private void installGson(String scriptPath, String entityName) {
        if(!builderConfig.containsKey(entityName)) throw new RuntimeException("Entity " + entityName + " not configured from build config");
        gsonObjectBuildProcessor.build(resourceLoadUtil.readDataFromFile(scriptPath), entityName, builderConfig);
    }
    
    private void installSql(String scriptPath) {
        sqlObjectBuildProcessor.build(resourceLoadUtil.readDataFromFile(scriptPath));
    }
    
    private void initObjectBuilderConfig() {
        String buildConfigContent = resourceLoadUtil.readDataFromFile(BUILD_CONFIG_PATH);
        if(StringUtils.isBlank(buildConfigContent)) throw new RuntimeException("Incorrect build config file -> " + BUILD_CONFIG_PATH);
        builderConfig = gsonGeneratorUtil.fromJsonToMap(buildConfigContent, ObjectBuilderProperty.class);
    }
}