package ru.mjs.install.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.springframework.stereotype.Controller;

/**
 * @author procki-u-a * *
 */
@Controller
public class ResourceLoadUtil {
    public String readDataFromFile(String fileName) {
        try {
            InputStream is = getClass().getResourceAsStream(fileName);
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append(" ");
            }
            br.close();
            isr.close();
            is.close();
            return sb.toString();
        } catch (Exception e) {
            throw new RuntimeException("Script not found from path -> " + fileName);
        }
    }
}
