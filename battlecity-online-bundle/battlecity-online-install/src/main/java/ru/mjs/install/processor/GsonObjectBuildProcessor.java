package ru.mjs.install.processor;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ru.mjs.dao.IInstallerDao;
import ru.mjs.install.util.GsonGeneratorUtil;
import ru.mjs.model.client.install.ObjectBuilderProperty;
import ru.mjs.model.database.UserAccountDO;
import ru.mjs.model.database.UserAccountRoleDO;

/**
 * @author procki-u-a * * 
 */
@Controller
public class GsonObjectBuildProcessor {
    @Autowired
    private IInstallerDao installerDao;
    @Autowired
    private GsonGeneratorUtil gsonGeneratorUtil;
    
    
    /**
     * Метод собирает DO объект из скрипта и сохраняет в БД
     * 
     * @param scriptContent - содержимое скрипта
     * @param entityName - имя сущности
     * @param builderConfig - конфиг билдера
     */
    public void build(String scriptContent, String entityName, Map<String, ObjectBuilderProperty> builderConfig) {
        try {
            Object object = Class.forName(builderConfig.get(entityName).getClassName()).newInstance();
            
            if(object instanceof UserAccountRoleDO) {
                List<UserAccountRoleDO> entitys = gsonGeneratorUtil.fromJsonToList(scriptContent, UserAccountRoleDO.class);
                
                for(UserAccountRoleDO entity : entitys) {
                    processEntity(entity, builderConfig, entityName);
                    installerDao.saveOrUpdate((UserAccountRoleDO)entity);
                }
                return;
            }
            
            if(object instanceof UserAccountDO) {
                List<UserAccountDO> entitys = gsonGeneratorUtil.fromJsonToList(scriptContent, UserAccountDO.class);
                
                for(UserAccountDO entity : entitys) {
                    processEntity(entity, builderConfig, entityName);
                    installerDao.saveOrUpdate((UserAccountDO) entity);
                }
                return;
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
 
    /**
     * Метод проверяющий уникальность сущности(включая вложения)
     * 
     * @param entity - обрабатываемая сущность
     * @param builderConfig - конфиг билдера
     * @param entityName - имя сущности
     */
    private void processEntity(Object entity, Map<String, ObjectBuilderProperty> builderConfig, String entityName) {
        if(!builderConfig.containsKey(entityName)) return;
        
        if(CollectionUtils.isNotEmpty(builderConfig.get(entityName).getNaturalKeys())) {
            prepareNaturalKeys(entity, builderConfig, entityName);
        }
        
        if(!builderConfig.get(entityName).getParams().isEmpty()) {
            prepareParameters(entity, builderConfig, entityName);
        }
    }
    
    /**
     * Метод обрабатывает вложенные сущности
     * 
     * @param entity - обрабатываемая сущность
     * @param builderConfig - конфиг билдера
     * @param entityName - имя сущности
     */
    private void prepareParameters(Object entity, Map<String, ObjectBuilderProperty> builderConfig, String entityName) {
        try {
            Map<String, String> params = builderConfig.get(entityName).getParams();
            
            for(String param : params.keySet()) {
                 Field field = getField(entity, param);
                 processEntity(field.get(entity) , builderConfig, params.get(param));
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Failed to get reflection type");
        }
    }
    
    /**
     * Метод обрабатывающий сущность по natural keys
     * 
     * @param entity - обрабатываемая сущность
     * @param builderConfig - конфиг билдера
     * @param entityName - имя сущности
     */
    private void prepareNaturalKeys(Object entity, Map<String, ObjectBuilderProperty> builderConfig, String entityName) {
        try {
            StringBuilder query = new StringBuilder();
            query.append("select o from ");
            query.append(entityName);
            query.append(" o where ");
            
            for(int i = 0; i < builderConfig.get(entityName).getNaturalKeys().size(); i++) {
                String naturalKey = builderConfig.get(entityName).getNaturalKeys().get(i);
                
                query.append("o.");
                query.append(naturalKey);
                query.append("='");
                
                Field field = getField(entity, naturalKey);
                
                query.append((String)field.get(entity));
                query.append("'");
                
                
                if(i+1 != builderConfig.get(entityName).getNaturalKeys().size()) {
                    query.append(" and ");
                }
            }
            
            Object persistentEntity = installerDao.getEntityByNaturalKeys(query.toString());
            
            if(persistentEntity != null) {
                changeId(entity, persistentEntity);
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Failed to get reflection type");
        }
    }
    
    /**
     * Метод корректирующий id сущности
     * 
     * @param currentEntity - обрабатываемая сущность
     * @param persistentEntity - сущность из БД
     */
    private void changeId(Object currentEntity, Object persistentEntity) {
        try {
            Field idCurrentEntityFied = getField(currentEntity, "id");
            Field idPersistentEntityField = getField(persistentEntity, "id");
            
            idCurrentEntityFied.set(currentEntity, idPersistentEntityField.get(persistentEntity));
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Change id failed");
        }
    }
    
    /**
     * Метод возвращает рефлекшен поле по имени
     * 
     * @param entity - сущность
     * @param fieldName - имя поля
     * @return рефлекшен поле
     */
    private Field getField(Object entity, String fieldName) {
        try {
            Field result = entity.getClass().getDeclaredField(fieldName);
            result.setAccessible(Boolean.TRUE);
            return result;
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Parameter not found: entity -> " + entity.toString() + " / fieldName -> " +fieldName);
        }
    }
}