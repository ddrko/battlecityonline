package ru.mjs.install;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import ru.mjs.install.core.BattlecityOnlineInstall;

/**
 * @author procki-u-a
 */
public class Main {
	private static final String SPRING_CONTEXT_PATH = "beans.xml";
	private static final String SERVER_BEAN = "battlecityOnlineInstall";
	
	
    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(SPRING_CONTEXT_PATH);
        BattlecityOnlineInstall battlecityOnlineInstall = (BattlecityOnlineInstall) ctx.getBean(SERVER_BEAN);
        System.out.println("--------------------------Start install--------------------------");
        battlecityOnlineInstall.install();
        System.out.println("--------------------------Stop install--------------------------");
    }
}
