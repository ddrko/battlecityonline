package ru.mjs.install.processor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ru.mjs.dao.IInstallerDao;

/**
 * @author procki-u-a * * 
 */
@Controller
public class SqlObjectBuildProcessor {
    @Autowired
    private IInstallerDao installerDao;
    
    /**
     * Метод накатывает sql скрипт на БД
     * 
     * @param scriptContent - содержимое скрипта
     */
    public void build(String scriptContent) {
        try {
            installerDao.executeSqlQuery(scriptContent);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}