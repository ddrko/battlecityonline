package ru.mjs.server.command.registration.convertor;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import ru.mjs.dao.IUserRoleDao;
import ru.mjs.model.client.enums.DataKeyEnum;
import ru.mjs.model.client.enums.ParameterCodeEnum;
import ru.mjs.model.client.enums.UserRoleTypeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.database.UserAccountDO;
import ru.mjs.server.common.IConvertor;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;

/**
 * @author procki-u-a * * 
 */
public class BattlecityOnlineRegistrationConvertorImpl implements IConvertor {
    @Autowired
    private IUserRoleDao userRoleDao;
    
    @Override
    public void convert(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException {
        UserAccountDO userAccount = new UserAccountDO();
        
        convertRegistrationDateValue(battlecityOnlineBaseHelper, userAccount);
        convertPasswordValue(battlecityOnlineBaseHelper, userAccount);
        convertLoginValue(battlecityOnlineBaseHelper, userAccount);
        convertDisabledValue(battlecityOnlineBaseHelper, userAccount);
        convertUserAccountRoleValue(battlecityOnlineBaseHelper, userAccount);
        convertNicknameValue(battlecityOnlineBaseHelper, userAccount);
        convertLastSignInDateValue(battlecityOnlineBaseHelper, userAccount);
        
        battlecityOnlineBaseHelper.getDataMap().put(DataKeyEnum.USER_ACCOUNT, userAccount);
    }

    private void convertLastSignInDateValue(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper, UserAccountDO userAccount) {
        userAccount.setLastSignInDate(new Date());
    }
    
    private void convertNicknameValue(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper, UserAccountDO userAccount) {
        userAccount.setNickname(battlecityOnlineBaseHelper.getPacketIn().getData().get(ParameterCodeEnum.NICKNAME.getName()));
    }
    
    private void convertUserAccountRoleValue(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper, UserAccountDO userAccount) {
        userAccount.setUserAccountRole(userRoleDao.findUserRoleByName(UserRoleTypeEnum.USER.getName()));
    }
    
    private void convertRegistrationDateValue(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper, UserAccountDO userAccount) {
        userAccount.setRegistrationDate(new Date());
    }
    
    private void convertPasswordValue(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper, UserAccountDO userAccount) {
        userAccount.setPassword(battlecityOnlineBaseHelper.getPacketIn().getData().get(ParameterCodeEnum.PASSWORD.getName()));
    }
    
    private void convertLoginValue(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper, UserAccountDO userAccount) {
        userAccount.setLogin(battlecityOnlineBaseHelper.getPacketIn().getData().get(ParameterCodeEnum.LOGIN.getName()));
    }
    
    private void convertDisabledValue(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper, UserAccountDO userAccount) {
        userAccount.setDisabled(Boolean.FALSE);
    }
}