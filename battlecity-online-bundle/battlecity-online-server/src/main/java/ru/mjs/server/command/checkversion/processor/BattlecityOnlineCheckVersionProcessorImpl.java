package ru.mjs.server.command.checkversion.processor;

import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.server.common.IProcessor;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;

/**
 * 
 * @author Frest1987
 *
 */
public class BattlecityOnlineCheckVersionProcessorImpl implements IProcessor {
	@Override
	public void process(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException {
		battlecityOnlineBaseHelper.getBattlecityOnlineServerSession().send(battlecityOnlineBaseHelper.getChannel(), battlecityOnlineBaseHelper.getPacketOut(), ReturnCodeEnum.RC_0001);
	}
}
