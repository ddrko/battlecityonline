package ru.mjs.server.core.validator;

import org.springframework.stereotype.Controller;

import ru.mjs.model.client.enums.ParameterCodeEnum;
import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;

/**
 * @author procki-u-a * * 
 */
@Controller
public class BattlecityOnlineCommandTypeValidator extends BattlecityOnlineBaseValidator {
    public void validate(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException {
        validateCommon(battlecityOnlineBaseHelper.getPacketIn(), ParameterCodeEnum.COMMAND_TYPE, ReturnCodeEnum.RC_0010);
    }
}
