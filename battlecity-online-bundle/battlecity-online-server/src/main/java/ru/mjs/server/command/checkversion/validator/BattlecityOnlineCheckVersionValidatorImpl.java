package ru.mjs.server.command.checkversion.validator;

import org.apache.commons.lang.StringUtils;

import ru.mjs.model.client.enums.ParameterCodeEnum;
import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.server.common.IValidator;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;
import ru.mjs.server.core.validator.BattlecityOnlineBaseValidator;

/**
 * 
 * @author Frest1987
 * 
 */
public class BattlecityOnlineCheckVersionValidatorImpl extends BattlecityOnlineBaseValidator implements IValidator {
	@Override
	public void validate(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException {
		validateCommon(battlecityOnlineBaseHelper.getPacketIn(), ParameterCodeEnum.SERVER_VERSION, ReturnCodeEnum.RC_0018);
		
		validateServerVersion(battlecityOnlineBaseHelper);
	}

	private void validateServerVersion(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException {
		if(!StringUtils.equals(battlecityOnlineBaseHelper.getBattlecityOnlineServerSession().getServerVersion(), battlecityOnlineBaseHelper.getPacketIn().getData().get(ParameterCodeEnum.SERVER_VERSION.getName()))) {
			throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0019);
		}
	}
}
