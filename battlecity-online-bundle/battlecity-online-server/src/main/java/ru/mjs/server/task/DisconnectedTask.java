package ru.mjs.server.task;

import org.jboss.netty.channel.Channel;

import ru.mjs.server.core.controller.BattlecityOnlineBaseController;

/**
 * @author procki-u-a * * 
 */
public class DisconnectedTask extends Thread {
    private BattlecityOnlineBaseController battlecityOnlineBaseController;
    private Channel channel;
    
    
    public DisconnectedTask(BattlecityOnlineBaseController battlecityOnlineBaseController, Channel channel) {
        this.battlecityOnlineBaseController = battlecityOnlineBaseController;
        this.channel = channel;
    }
    
    @Override
    public void run() {
        battlecityOnlineBaseController.processDisconnectedChannel(channel);
    }
}
