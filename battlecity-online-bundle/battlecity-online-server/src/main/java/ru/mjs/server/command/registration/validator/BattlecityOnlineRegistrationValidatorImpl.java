package ru.mjs.server.command.registration.validator;

import ru.mjs.model.client.enums.ParameterCodeEnum;
import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.server.common.IValidator;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;
import ru.mjs.server.core.validator.BattlecityOnlineBaseValidator;

/**
 * @author procki-u-a * * 
 */
public class BattlecityOnlineRegistrationValidatorImpl extends BattlecityOnlineBaseValidator implements IValidator {
    @Override
    public void validate(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException {
        validateCommon(battlecityOnlineBaseHelper.getPacketIn(), ParameterCodeEnum.NICKNAME, ReturnCodeEnum.RC_0012);
        validateCommon(battlecityOnlineBaseHelper.getPacketIn(), ParameterCodeEnum.LOGIN, ReturnCodeEnum.RC_0013);
        validateCommon(battlecityOnlineBaseHelper.getPacketIn(), ParameterCodeEnum.PASSWORD, ReturnCodeEnum.RC_0014);
    }
}
