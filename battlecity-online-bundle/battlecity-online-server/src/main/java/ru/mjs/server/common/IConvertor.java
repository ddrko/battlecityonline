package ru.mjs.server.common;

import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;

/**
 * @author procki-u-a * * 
 */
public interface IConvertor {
    public void convert(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException;
}
