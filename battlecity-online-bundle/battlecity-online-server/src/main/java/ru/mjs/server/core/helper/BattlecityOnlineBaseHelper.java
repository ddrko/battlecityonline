package ru.mjs.server.core.helper;

import java.util.Map;

import org.jboss.netty.channel.Channel;

import com.google.common.collect.Maps;

import lombok.Data;
import ru.mjs.model.client.enums.DataKeyEnum;
import ru.mjs.model.client.protocol.Packet;
import ru.mjs.server.core.session.BattlecityOnlineServerSession;

/**
 * @author procki-u-a * * 
 */
@Data
public class BattlecityOnlineBaseHelper {
    private Channel channel;
    private BattlecityOnlineServerSession battlecityOnlineServerSession;
    private Packet packetIn = new Packet();
    private Packet packetOut = new Packet();
    private Map<DataKeyEnum, Object> dataMap = Maps.newHashMap();
    
    public BattlecityOnlineBaseHelper(Channel channel, BattlecityOnlineServerSession battlecityOnlineServerSession) {
        this.channel = channel;
        this.battlecityOnlineServerSession = battlecityOnlineServerSession;
    }
}
