package ru.mjs.server.common;

import java.util.Map;

import ru.mjs.model.client.enums.CommandCodeEnum;
import ru.mjs.model.client.enums.ParameterCodeEnum;
import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.client.protocol.Packet;

/**
 * Фабрика выдающая контрол в зависимости от комманды
 * 
 * @author procki-u-a * *
 */
public class CommandControllerFactory {
	private Map<CommandCodeEnum, IController> controllers;
	
	public IController getController(Packet packetIn) throws BattlecityOnlineBaseException {
		IController result = controllers.get(CommandCodeEnum.find(packetIn.getData().get(ParameterCodeEnum.COMMAND_TYPE.getName())));
		
		if(result == null) {
			throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0011);
		}
		
		return result;
	}

    public void setControllers(Map<CommandCodeEnum, IController> controllers) {
        this.controllers = controllers;
    }
}
