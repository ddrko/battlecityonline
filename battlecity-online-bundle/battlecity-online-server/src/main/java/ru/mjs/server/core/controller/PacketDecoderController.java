package ru.mjs.server.core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.client.protocol.Packet;
import ru.mjs.server.util.GsonGeneratorUtil;

/**
 * @author procki-u-a * *
 */
@Controller
public class PacketDecoderController {
    @Autowired
    private GsonGeneratorUtil gsonGeneratorUtil;

    public String encodePacket(Packet packet) throws BattlecityOnlineBaseException {
        try {
            return gsonGeneratorUtil.toJson(packet);
        } catch (Exception e) {
            throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0003);
        }
    }

    public Packet decodePacket(String request) throws BattlecityOnlineBaseException {
        try {
            return gsonGeneratorUtil.fromJsonToObject(request, Packet.class);
        } catch (Exception e) {
            throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0003);
        }
    }
}
