package ru.mjs.server.core;

import static ru.mjs.server.util.JansiConsoleLoggerUtil.printGreen;
import static ru.mjs.server.util.JansiConsoleLoggerUtil.printRed;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ru.mjs.server.core.nio.pipeline.BattlecityOnlineServerPipelineFactory;

/**
 * Инициализация сервера
 * 
 * @author procki-u-a
 */
@Component
public class BattlecityOnlineServer {
	@Autowired
	private BattlecityOnlineServerPipelineFactory battlecityOnlineServerPipelineFactory;
	
	@Value("${server.host}")
	private String serverHost;
	@Value("${server.port}")
    private String serverPort;
	
	public void initServer() {
		printGreen("CONTEXT LOAD STATUS -> OK");
		   
		try {
	        ServerBootstrap bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));               
	        bootstrap.setPipelineFactory(battlecityOnlineServerPipelineFactory);
	        bootstrap.bind(initSocketAddress());    
	        
	        printGreen("SERVER START STATUS -> OK");
		} catch (Exception e) {
            printRed("SERVER START STATUS -> FAILED");
        }
	}
	
	private InetSocketAddress initSocketAddress() throws UnknownHostException {
	    printGreen(String.format("BIND SERVER TO HOST -> %s  / PORT -> %s", serverHost, serverPort));
	    
		InetAddress inetAddress = InetAddress.getByName(serverHost);
		InetSocketAddress inetSocketAddress = new InetSocketAddress(inetAddress, Integer.valueOf(serverPort));
		return inetSocketAddress;
	}
}