package ru.mjs.server.common;

import java.util.List;

import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;

/**
 * Интерфейс контроллера
 * 
 * @author procki-u-a * *
 */
public interface IController {
	public void process(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException;
	
	public void setValidators(List<IValidator> validators);
	public void setConvertors(List<IConvertor> convertors);
	public void setProcessors(List<IProcessor> processors);
}
