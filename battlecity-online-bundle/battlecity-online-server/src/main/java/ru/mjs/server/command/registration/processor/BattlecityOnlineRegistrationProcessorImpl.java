package ru.mjs.server.command.registration.processor;

import org.springframework.beans.factory.annotation.Autowired;

import ru.mjs.dao.IUserAccountDao;
import ru.mjs.model.client.enums.DataKeyEnum;
import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.database.UserAccountDO;
import ru.mjs.server.common.IProcessor;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;

/**
 * @author procki-u-a * * 
 */
public class BattlecityOnlineRegistrationProcessorImpl implements IProcessor {
    @Autowired
    private IUserAccountDao userAccountDao;
    
    @Override
    public void process(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException {
        UserAccountDO userAccount = (UserAccountDO)battlecityOnlineBaseHelper.getDataMap().get(DataKeyEnum.USER_ACCOUNT);
        try {
            userAccountDao.save(userAccount);
        } catch (Exception e) {
            throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0016);
        }
        
        battlecityOnlineBaseHelper.getBattlecityOnlineServerSession().send(battlecityOnlineBaseHelper.getChannel(), battlecityOnlineBaseHelper.getPacketOut(), ReturnCodeEnum.RC_0001);
    }    
}
