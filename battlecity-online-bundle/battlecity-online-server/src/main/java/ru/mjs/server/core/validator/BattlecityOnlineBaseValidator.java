package ru.mjs.server.core.validator;

import org.apache.commons.lang.StringUtils;

import ru.mjs.model.client.enums.ParameterCodeEnum;
import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.client.protocol.Packet;

/**
 * @author procki-u-a * * 
 */
public abstract class BattlecityOnlineBaseValidator {
    public void validateCommon(Packet packet, ParameterCodeEnum parameterCodeEnum, ReturnCodeEnum returnCodeEnum) throws BattlecityOnlineBaseException {
        if(null == packet) throw new BattlecityOnlineBaseException(returnCodeEnum);
        if(!packet.getData().containsKey(parameterCodeEnum.getName())) throw new BattlecityOnlineBaseException(returnCodeEnum);
        if(StringUtils.isBlank(packet.getData().get(parameterCodeEnum.getName()))) throw new BattlecityOnlineBaseException(returnCodeEnum);
    }
}
