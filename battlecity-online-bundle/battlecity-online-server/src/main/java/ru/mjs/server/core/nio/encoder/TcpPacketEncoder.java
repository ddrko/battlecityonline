package ru.mjs.server.core.nio.encoder;

import static org.jboss.netty.buffer.ChannelBuffers.copiedBuffer;

import java.nio.charset.Charset;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

/**
 * @author procki-u-a * * 
 */
public class TcpPacketEncoder extends OneToOneEncoder {
    private final Charset charset;
    
    public TcpPacketEncoder() {
        this(Charset.defaultCharset());
    }
    
    public TcpPacketEncoder(Charset charset) {
        if(charset == null) {
            throw new NullPointerException("TcpPacketEncoder charset is null");
        }
        this.charset = charset;
    }
    
    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
        return copiedBuffer(ctx.getChannel().getConfig().getBufferFactory().getDefaultOrder(), (String) msg, charset);
    }
}
