package ru.mjs.server.command.login.convertor;

import org.springframework.beans.factory.annotation.Autowired;

import ru.mjs.dao.IUserAccountDao;
import ru.mjs.model.client.enums.DataKeyEnum;
import ru.mjs.model.client.enums.ParameterCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.database.UserAccountDO;
import ru.mjs.server.common.IConvertor;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;

/**
 * @author procki-u-a * * 
 */
public class BattlecityOnlineLogInConvertorImpl implements IConvertor {
    @Autowired
    private IUserAccountDao userAccountDao;
    
    @Override
    public void convert(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException {
        String login = battlecityOnlineBaseHelper.getPacketIn().getData().get(ParameterCodeEnum.LOGIN.getName());
        String password = battlecityOnlineBaseHelper.getPacketIn().getData().get(ParameterCodeEnum.PASSWORD.getName());
        
        UserAccountDO userAccount = userAccountDao.findUserAccount(login, password);
        battlecityOnlineBaseHelper.getDataMap().put(DataKeyEnum.USER_ACCOUNT, userAccount);
    }
}
