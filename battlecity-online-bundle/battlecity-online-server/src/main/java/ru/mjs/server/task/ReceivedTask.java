package ru.mjs.server.task;

import org.jboss.netty.channel.Channel;

import ru.mjs.model.client.protocol.Packet;
import ru.mjs.server.core.controller.BattlecityOnlineBaseController;

/**
 * @author procki-u-a * * 
 */
public class ReceivedTask extends Thread {
    private BattlecityOnlineBaseController battlecityOnlineBaseController;
    private Channel channel;
    private String packet;
    
    
    public ReceivedTask(BattlecityOnlineBaseController battlecityOnlineBaseController, Channel channel, String packet) {
        this.battlecityOnlineBaseController = battlecityOnlineBaseController;
        this.channel = channel;
        this.packet = packet;
    }
    
    @Override
    public void run() {
        battlecityOnlineBaseController.processReceivedMessage(channel, packet);
    }
}
