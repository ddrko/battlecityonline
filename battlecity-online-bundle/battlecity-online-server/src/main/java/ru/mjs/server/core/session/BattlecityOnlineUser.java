package ru.mjs.server.core.session;

import lombok.Data;

import org.jboss.netty.channel.Channel;

import ru.mjs.model.database.UserAccountDO;

/**
 * @author procki-u-a * * 
 */
@Data
public class BattlecityOnlineUser {
    private volatile UserAccountDO userAccount;
    private volatile Channel channel;
    private volatile BattlecityOnlineLobby battlecityOnlineLobby;
    
    
    public BattlecityOnlineUser(UserAccountDO userAccount, Channel channel) {
        this.userAccount = userAccount;
        this.channel = channel;
    }
}
