package ru.mjs.server.core.nio.handler;

import lombok.extern.slf4j.Slf4j;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import ru.mjs.model.client.protocol.Packet;
import ru.mjs.server.core.controller.BattlecityOnlineBaseController;
import ru.mjs.server.task.DisconnectedTask;
import ru.mjs.server.task.ReceivedTask;

/**
 * Обработчик входящий соединений
 * 
 * @author procki-u-a * *
 */
@Slf4j
@Component
@Qualifier("battlecityOnlineServerHandler")
public class BattlecityOnlineServerHandler extends SimpleChannelUpstreamHandler {
    @Autowired
    private BattlecityOnlineBaseController battlecityOnlineBaseController;
    
    
    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        String packet = (String) e.getMessage();
        
        log.info(String.format("Client received -> id:%s / address:%s / request:%s", ctx.getChannel().getId(), ctx.getChannel().getLocalAddress(), packet.toString()));
        
        ctx.getChannel().write(packet);
        
        //new ReceivedTask(battlecityOnlineBaseController, ctx.getChannel(), packet).start();
    }
    
    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        log.info(String.format("Client connected -> id:%s / address:%s", ctx.getChannel().getId(), ctx.getChannel().getLocalAddress()));
        super.channelConnected(ctx, e);
    }
    
    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        log.info(String.format("Client disconnected -> id:%s / address:%s", ctx.getChannel().getId(), ctx.getChannel().getLocalAddress()));
        new DisconnectedTask(battlecityOnlineBaseController, ctx.getChannel()).start();
        super.channelDisconnected(ctx, e);
    }
    
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        log.error(e.toString());
        e.getChannel().close();
    }
}