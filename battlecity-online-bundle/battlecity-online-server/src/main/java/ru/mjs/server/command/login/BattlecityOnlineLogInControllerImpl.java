package ru.mjs.server.command.login;

import java.util.List;

import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.server.common.IController;
import ru.mjs.server.common.IConvertor;
import ru.mjs.server.common.IProcessor;
import ru.mjs.server.common.IValidator;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;

/**
 * Контроллер логина юзера на сервере
 * 
 * @author procki-u-a * * 
 */
public class BattlecityOnlineLogInControllerImpl implements IController {
    private List<IValidator> validators;
    private List<IConvertor> convertors;
    private List<IProcessor> processors;
    

    @Override
    public void process(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException {
        for(IValidator validator : validators) {
            validator.validate(battlecityOnlineBaseHelper);
        }
        
        for(IConvertor convertor : convertors) {
            convertor.convert(battlecityOnlineBaseHelper);
        }
        
        for(IProcessor processor : processors) {
            processor.process(battlecityOnlineBaseHelper);
        }
    }
    
    @Override
    public void setValidators(List<IValidator> validators) {
        this.validators = validators;
    }
    
    @Override
    public void setConvertors(List<IConvertor> convertors) {
        this.convertors = convertors;
    }
    
    @Override
    public void setProcessors(List<IProcessor> processors) {
        this.processors = processors;
    }
}