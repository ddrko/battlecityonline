package ru.mjs.server.core.session;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang.StringUtils;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.springframework.beans.factory.annotation.Autowired;

import ru.mjs.model.client.enums.ParameterCodeEnum;
import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.client.protocol.Packet;
import ru.mjs.model.database.UserAccountDO;
import ru.mjs.server.core.controller.PacketDecoderController;

import com.google.common.collect.Maps;

/**
 * @author procki-u-a * * 
 */
public class BattlecityOnlineServerSession {
    @Autowired
    private PacketDecoderController packetDecoderController;
    
    @Getter
    @Setter
    private String serverVersion;
    @Getter
    @Setter
    private Integer maxUserCount;
    private volatile Map<Integer, BattlecityOnlineUser> users = Maps.newHashMap();
    private volatile Map<String, BattlecityOnlineLobby> lobbys = Maps.newHashMap();
    
    
    public synchronized void addUser(UserAccountDO userAccount, Channel channel) throws BattlecityOnlineBaseException {
        if(users.size() >= maxUserCount) throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0005);
        users.put(channel.getId(), new BattlecityOnlineUser(userAccount, channel));
    }
    
    public synchronized void removeUser(Channel channel) {
        BattlecityOnlineUser user = users.remove(channel.getId());
        
        if(null == user) return;
        
        BattlecityOnlineLobby lobby = user.getBattlecityOnlineLobby();
        
        if(null == lobby) return;
        
        lobby.getLobbyUsers().remove(channel.getId());
    }
    
    public synchronized void send(Channel channel, Packet packet, ReturnCodeEnum returnCodeEnum) {
        channel.write(new TextWebSocketFrame(packetDecoderController.encodePacket(fillBaseParametersToResponse(packet, returnCodeEnum))));
    }
    
    public synchronized void sendAll(Channel channel, Packet packet, ReturnCodeEnum returnCodeEnum, String returnMessage) {
        for(Integer userKey: users.keySet()) {
            if(userKey == channel.getId()) continue;
            users.get(userKey).getChannel().write(new TextWebSocketFrame(packetDecoderController.encodePacket(fillBaseParametersToResponse(packet, returnCodeEnum))));
        }
    }
    
    public synchronized void sendAllInLobby(Channel channel, Packet packet, ReturnCodeEnum returnCodeEnum, String returnMessage) throws BattlecityOnlineBaseException {
        if(!users.containsKey(channel.getId())) throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0004);
        
        BattlecityOnlineLobby lobby = users.get(channel.getId()).getBattlecityOnlineLobby();
        
        if(null == lobby) throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0004);
        
        for(Integer userKey: lobby.getLobbyUsers().keySet()) {
            if(userKey == channel.getId()) continue;
            lobby.getLobbyUsers().get(userKey).getChannel().write(new TextWebSocketFrame(packetDecoderController.encodePacket(fillBaseParametersToResponse(packet, returnCodeEnum))));
        }
    }
    
    public synchronized void addLobby(Channel channel, String lobbyName, String lobbyPassword, Integer maxUserCount) throws BattlecityOnlineBaseException {
        if(!users.containsKey(channel.getId())) throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0006);
        
        BattlecityOnlineUser user = users.get(channel.getId());
        BattlecityOnlineLobby lobby = new BattlecityOnlineLobby(lobbyName, lobbyPassword, maxUserCount);
        
        user.setBattlecityOnlineLobby(lobby);
        lobby.getLobbyUsers().put(channel.getId(), user);
        
        lobbys.put(lobbyName, lobby);
    }
    
    public synchronized void joinToLobby(Channel channel, String lobbyName, String lobbyPassword) throws BattlecityOnlineBaseException {
        if(!lobbys.containsKey(lobbyName)) throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0007);
        
        BattlecityOnlineLobby lobby = lobbys.get(lobbyName);
        
        if(lobby.getMaxUserCount() >= lobby.getLobbyUsers().size()) throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0008);
        if(StringUtils.isNotBlank(lobby.getLobbyPassword()) && !StringUtils.equals(lobby.getLobbyPassword(), lobbyPassword)) throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0009);
        
        BattlecityOnlineUser user = users.get(channel.getId());
        user.setBattlecityOnlineLobby(lobby);
        lobby.getLobbyUsers().put(channel.getId(), user);
    }
    
    
    private Packet fillBaseParametersToResponse(Packet packet, ReturnCodeEnum returnCodeEnum) {
        packet.getData().put(ParameterCodeEnum.RETURN_CODE.getName(), returnCodeEnum.getName());
        return packet;
    }
}