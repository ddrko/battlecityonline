package ru.mjs.server.core.nio.pipeline;

import java.nio.charset.Charset;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.handler.codec.frame.DelimiterBasedFrameDecoder;
import org.jboss.netty.handler.codec.frame.Delimiters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ru.mjs.server.core.nio.decoder.TcpPacketDecoder;
import ru.mjs.server.core.nio.encoder.TcpPacketEncoder;
import ru.mjs.server.core.nio.handler.BattlecityOnlineServerHandler;

/**
 * Класс настраивающий протокол сервера
 * 
 * @author procki-u-a * * 
 */
@Component
public class BattlecityOnlineServerPipelineFactory implements ChannelPipelineFactory {
    @Autowired
    private BattlecityOnlineServerHandler battlecityOnlineServerHandler;
    
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline pipeline = Channels.pipeline();
        
        pipeline.addLast("framer", new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
        pipeline.addLast("decoder", new TcpPacketDecoder(Charset.forName("utf8")));
        pipeline.addLast("encoder", new TcpPacketEncoder(Charset.forName("utf8")));
        pipeline.addLast("handler", battlecityOnlineServerHandler);
        return pipeline;
    }
}