package ru.mjs.server.core.controller;

import org.jboss.netty.channel.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.client.protocol.Packet;
import ru.mjs.server.common.CommandControllerFactory;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;
import ru.mjs.server.core.session.BattlecityOnlineServerSession;
import ru.mjs.server.core.validator.BattlecityOnlineCommandTypeValidator;

/**
 * Базовый контроллер
 * 
 * @author procki-u-a * *
 */
@Controller
public class BattlecityOnlineBaseController {
    @Autowired
    private CommandControllerFactory commandControllerFactory;
    @Autowired
    private PacketDecoderController packetDecoderController;
    @Autowired
    private BattlecityOnlineServerSession battlecityOnlineServerSession;
    @Autowired
    private BattlecityOnlineCommandTypeValidator battlecityOnlineCommandTypeValidator;
    
    
    
    public void processReceivedMessage(Channel channel, String packet) {
        BattlecityOnlineBaseHelper battlecityOnlineBaseHelper = new BattlecityOnlineBaseHelper(channel, battlecityOnlineServerSession);
        
//        try {
//            battlecityOnlineBaseHelper.setPacketIn(packetDecoderController.decodePacket(""));
//            battlecityOnlineCommandTypeValidator.validate(battlecityOnlineBaseHelper);
//            commandControllerFactory.getController(battlecityOnlineBaseHelper.getPacketIn()).process(battlecityOnlineBaseHelper);
//        } catch (BattlecityOnlineBaseException e) {
//            battlecityOnlineServerSession.send(channel, battlecityOnlineBaseHelper.getPacketOut(), e.getReturnCodeEnum());
//        } catch (Exception e) {
//            battlecityOnlineServerSession.send(channel, battlecityOnlineBaseHelper.getPacketOut(), ReturnCodeEnum.RC_0002);
//        }
    }

    public void processDisconnectedChannel(Channel channel) {
       battlecityOnlineServerSession.removeUser(channel);
    }
}