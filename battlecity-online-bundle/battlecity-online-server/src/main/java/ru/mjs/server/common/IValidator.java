package ru.mjs.server.common;

import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;

/**
 * @author procki-u-a * * 
 */
public interface IValidator {
    public void validate(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException;
}
