package ru.mjs.server.util;

import static org.fusesource.jansi.Ansi.ansi;
import static org.fusesource.jansi.Ansi.Color.GREEN;
import static org.fusesource.jansi.Ansi.Color.BLUE;
import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.Color.YELLOW;
import static org.fusesource.jansi.Ansi.Color.WHITE;

import org.fusesource.jansi.AnsiConsole;

/**
 * Класс подсвечивающий вывод в консоли
 * 
 * @author procki-u-a
 */
public class JansiConsoleLoggerUtil {
	
	public static void printRed(String printText) {
		AnsiConsole.systemInstall();
		System.out.println(ansi().fg(RED).a(printText).reset());
	}
	
	public static void printGreen(String printText) {
		AnsiConsole.systemInstall();
		System.out.println(ansi().fg(GREEN).a(printText).reset());
	}
	
	public static void printBlue(String printText) {
		AnsiConsole.systemInstall();
		System.out.println(ansi().fg(BLUE).a(printText).reset());
	}
	
	public static void printYellow(String printText) {
		AnsiConsole.systemInstall();
		System.out.println(ansi().fg(YELLOW).a(printText).reset());
	}
	
	public static void printWhite(String printText) {
		AnsiConsole.systemInstall();
		System.out.println(ansi().fg(WHITE).a(printText).reset());
	}
}
