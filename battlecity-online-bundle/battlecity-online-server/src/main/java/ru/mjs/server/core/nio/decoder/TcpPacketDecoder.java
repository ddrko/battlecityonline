package ru.mjs.server.core.nio.decoder;

import java.nio.charset.Charset;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneDecoder;

/**
 * Дэкодер TCP пакетов
 * 
 * @author procki-u-a * * 
 */
public class TcpPacketDecoder extends OneToOneDecoder {
    private final Charset charset;
    
    public TcpPacketDecoder() {
        this(Charset.defaultCharset());
    }
    
    public TcpPacketDecoder(Charset charset) {
        if(charset == null) {
            throw new NullPointerException("TcpPacketDecoder charset is null");
        }
        this.charset = charset;
    }
    
    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
        if(!(msg instanceof ChannelBuffer)) {
            return msg;
        }
        
        return ((ChannelBuffer)msg).toString(this.charset);
    }
}