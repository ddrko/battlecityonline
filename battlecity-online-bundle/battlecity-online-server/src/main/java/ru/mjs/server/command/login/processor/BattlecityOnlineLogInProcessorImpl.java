package ru.mjs.server.command.login.processor;

import ru.mjs.model.client.enums.DataKeyEnum;
import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.database.UserAccountDO;
import ru.mjs.server.common.IProcessor;
import ru.mjs.server.core.helper.BattlecityOnlineBaseHelper;

/**
 * @author procki-u-a * * 
 */
public class BattlecityOnlineLogInProcessorImpl implements IProcessor {
    @Override
    public void process(BattlecityOnlineBaseHelper battlecityOnlineBaseHelper) throws BattlecityOnlineBaseException {
        UserAccountDO userAccount = (UserAccountDO)battlecityOnlineBaseHelper.getDataMap().get(DataKeyEnum.USER_ACCOUNT);

        battlecityOnlineBaseHelper.getBattlecityOnlineServerSession().addUser(userAccount, battlecityOnlineBaseHelper.getChannel());
        battlecityOnlineBaseHelper.getBattlecityOnlineServerSession().send(battlecityOnlineBaseHelper.getChannel(), battlecityOnlineBaseHelper.getPacketOut(), ReturnCodeEnum.RC_0001);
    }
}
