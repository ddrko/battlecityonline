package ru.mjs.server.util;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Controller;

import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

/**
 * Класс конвертер gson->object, object->gson
 * 
 * @author procki-u-a * * 
 */
@Controller
public class GsonGeneratorUtil implements InitializingBean {
    private Gson gson;
    
    @Override
    public void afterPropertiesSet() throws Exception {
        gson = new Gson();
    }
    
    public String toJson(Object object) {
        return gson.toJson(object);
    }
    
    public <T>T fromJsonToObject(String content, Class<T> clazz) {
        return gson.fromJson(content, clazz);
    }
    
    @SuppressWarnings("serial")
    public <T>List<T> fromJsonToList(String content, Class<T> clazz) {
        Type type = new TypeToken<ArrayList<T>>(){}.where(new TypeParameter<T>(){}, clazz).getType();
        return gson.fromJson(content, type);
    }
    
    @SuppressWarnings("serial")
    public <T>Map<String, T> fromJsonToMap(String content, Class<T> clazz){
        Type type = new TypeToken<HashMap<String, T>>(){}.where(new TypeParameter<T>(){}, clazz).getType();

        return gson.fromJson(content, type);
    }
}