package ru.mjs.server;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import ru.mjs.server.core.BattlecityOnlineServer;

/**
 * Точка входа приложения, 
 * загрузка контекста, 
 * инициализация сервера.
 * 
 * @author procki-u-a
 */
public class Main {
	private static final String SPRING_CONTEXT_PATH = "beans.xml";
	private static final String SERVER_BEAN = "battlecityOnlineServer";
	
	
    public static void main( String[] args ) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(SPRING_CONTEXT_PATH);
        BattlecityOnlineServer battlecityOnlineServer = (BattlecityOnlineServer) ctx.getBean(SERVER_BEAN);
        battlecityOnlineServer.initServer();
    }
}