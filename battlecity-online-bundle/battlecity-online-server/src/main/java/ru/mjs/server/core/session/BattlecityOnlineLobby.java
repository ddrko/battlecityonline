package ru.mjs.server.core.session;

import java.util.Map;

import lombok.Data;

import com.google.common.collect.Maps;

/**
 * @author procki-u-a * * 
 */
@Data
public class BattlecityOnlineLobby {
    private volatile Integer maxUserCount = 0;
    private volatile String lobbyName;
    private volatile String lobbyPassword;
    private volatile Map<Integer, BattlecityOnlineUser> lobbyUsers = Maps.newHashMap();
    
    public BattlecityOnlineLobby(String lobbyName, String lobbyPassword, Integer maxUserCount) {
        this.lobbyName = lobbyName;
        this.lobbyPassword = lobbyPassword;
        this.maxUserCount = maxUserCount;
    }
}
