package ru.mjs.server.common;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ru.mjs.model.client.enums.CommandCodeEnum;
import ru.mjs.model.client.enums.ParameterCodeEnum;
import ru.mjs.model.client.protocol.Packet;
import ru.mjs.server.BaseTest;
import ru.mjs.server.command.login.BattlecityOnlineLogInControllerImpl;
import ru.mjs.server.command.registration.BattlecityOnlineRegistrationControllerImpl;

/**
 * @author procki-u-a * * 
 */
public class CommandControllerFactoryTest extends BaseTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private CommandControllerFactory controllerFactory;
    
    
    
    @Test
    public void testGetController() {
        logger.info("testGetController");
        
        assertTrue(controllerFactory.getController(createPacket(CommandCodeEnum.LOG_IN)) instanceof BattlecityOnlineLogInControllerImpl);
        assertTrue(controllerFactory.getController(createPacket(CommandCodeEnum.REGISTRATION)) instanceof BattlecityOnlineRegistrationControllerImpl);
    }
    
    
    private Packet createPacket(CommandCodeEnum commandCodeEnum) {
        Packet packet = new Packet();
        
        packet.setC2dictionary(Boolean.TRUE);
        packet.getData().put(ParameterCodeEnum.COMMAND_TYPE.getName(), commandCodeEnum.getName());
        return packet;
    }
}