package ru.mjs.server.dao;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ru.mjs.dao.IUserAccountDao;
import ru.mjs.dao.IUserRoleDao;
import ru.mjs.model.client.enums.UserRoleTypeEnum;
import ru.mjs.model.database.UserAccountDO;
import ru.mjs.model.database.UserAccountRoleDO;
import ru.mjs.server.BaseTest;

/**
 * @author procki-u-a * * 
 */
public class UserAccountDaoImplTest extends BaseTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private IUserAccountDao userAccountDao;
    @Autowired
    private IUserRoleDao userRoleDao;
    
    
    @Test
    @Transactional
    public void testSave() {
        logger.info("testSave");
        
        userAccountDao.save(createUserAccount());
    }
    
    @Test
    @Transactional
    public void testUpdate() {
        logger.info("testUpdate");
        
        UserAccountDO userAccount = createUserAccount();
        userAccountDao.save(userAccount);
        
        userAccount.setLogin("test1");
        userAccountDao.update(userAccount);
    }
    
    @Test
    @Transactional
    public void testFindUserAccount() {
        logger.info("testFindUserAccount");
        
        userAccountDao.save(createUserAccount());
        
        assertNotNull(userAccountDao.findUserAccount("test", "test"));
    }
    
    
    private UserAccountDO createUserAccount() {
        UserAccountDO result = new UserAccountDO();
        
        result.setDisabled(Boolean.FALSE);
        result.setLastSignInDate(new Date());
        result.setLogin("test");
        result.setNickname("test");
        result.setPassword("test");
        result.setRegistrationDate(new Date());
        result.setUserAccountRole(createUserAccountRole());
        
        return result;
    }
    
    private UserAccountRoleDO createUserAccountRole() {
        return userRoleDao.findUserRoleByName(UserRoleTypeEnum.USER.getName());
    }
}