package ru.mjs.server.testclient;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TcpClient {
    
    
    public static void main(String[] args) throws IOException {
        log.info("test");
        
        Socket client = new Socket("localhost", 10500);
        client.setSoTimeout(50000);
        
        BufferedReader inFromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
        DataOutputStream outToClient = new DataOutputStream(client.getOutputStream());
        outToClient.writeBytes("test message \n");
        
        log.info(inFromClient.readLine());
        
        outToClient.close();
        inFromClient.close();
        client.close();
    }
}
