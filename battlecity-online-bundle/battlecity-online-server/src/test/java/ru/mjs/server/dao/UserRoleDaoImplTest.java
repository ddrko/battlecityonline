package ru.mjs.server.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ru.mjs.dao.IUserRoleDao;
import ru.mjs.model.client.enums.UserRoleTypeEnum;
import ru.mjs.server.BaseTest;

/**
 * @author procki-u-a * * 
 */
public class UserRoleDaoImplTest extends BaseTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private IUserRoleDao userRoleDao;

    @Test
    @Transactional
    public void testGetAllUserRoles() {
        logger.info("testGetAllUserRoles");
        
        assertFalse(CollectionUtils.isEmpty(userRoleDao.getAllUserRoles()));
    }

    @Test
    @Transactional
    public void testFindUserRoleByName() {
        logger.info("testFindUserRoleByName");
        
        assertNotNull(userRoleDao.findUserRoleByName(UserRoleTypeEnum.USER.getName()));
    }
}
