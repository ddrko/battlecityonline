package ru.mjs.server.core.controller;

import static org.junit.Assert.*;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ru.mjs.model.client.enums.ParameterCodeEnum;
import ru.mjs.model.client.protocol.Packet;
import ru.mjs.server.BaseTest;

/**
 * @author procki-u-a * * 
 */
public class PacketDecoderControllerTest extends BaseTest {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    private static final String GSON_LINE = "{\"c2dictionary\":true,\"data\":{\"LOGIN\":\"login\",\"RETURN_CODE\":\"return code\",\"PASSWORD\":\"password\"}}";
    
    @Autowired
    private PacketDecoderController decoderController;
    
    
    @Test
    public void testEncodePacket() {
        logger.info("testEncodePacket");
        
        String result = decoderController.encodePacket(createPacket());
        assertTrue(StringUtils.equals(result, GSON_LINE));
    }

    @Test
    public void testDecodePacket() {
        logger.info("testDecodePacket");
        
        Packet result = decoderController.decodePacket(GSON_LINE);
        
        assertNotNull(result);
        assertTrue(result.getC2dictionary());
        assertTrue(result.getData().containsKey(ParameterCodeEnum.LOGIN.getName()));
        assertTrue(result.getData().containsKey(ParameterCodeEnum.PASSWORD.getName()));
        assertTrue(result.getData().containsKey(ParameterCodeEnum.RETURN_CODE.getName()));
    }
    
    private Packet createPacket() {
        Packet packet = new Packet();
        
        packet.setC2dictionary(Boolean.TRUE);
        packet.getData().put(ParameterCodeEnum.LOGIN.getName(), "login");
        packet.getData().put(ParameterCodeEnum.PASSWORD.getName(), "password");
        packet.getData().put(ParameterCodeEnum.RETURN_CODE.getName(), "return code");
        
        return packet;
    }
}