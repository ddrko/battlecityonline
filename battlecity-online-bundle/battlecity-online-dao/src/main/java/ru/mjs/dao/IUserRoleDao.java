package ru.mjs.dao;

import java.util.List;

import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.database.UserAccountRoleDO;

/**
 * @author procki-u-a * * 
 */
public interface IUserRoleDao {
    public List<UserAccountRoleDO> getAllUserRoles() throws BattlecityOnlineBaseException;
    public UserAccountRoleDO findUserRoleByName(String userRoleName) throws BattlecityOnlineBaseException;
}
