package ru.mjs.dao.cache;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;

import org.springframework.stereotype.Controller;

@Controller
public class BattlecityOnlineCacheManager {
	@SuppressWarnings("unchecked")
	public <T> T getElement(String cache, Object key) {
		Ehcache ehcache = getCache(cache);
		
		if(ehcache == null) {
			return null;
		}
		
		Element element = ehcache.get(key);

		if (element != null) {
			return (T) element.getValue();
		}

		return null;
	}

	public void addElement(String cache, Object key, Object value) {
		Ehcache ehcache = getCache(cache);
		
		if(ehcache != null) {
			ehcache.put(new Element(key, value));
		}
	}

	public boolean removeElement(String cache, Object key) {
		Ehcache ehcache = getCache(cache);
		
		if(ehcache == null) {
			return Boolean.FALSE;
		}
		
		return ehcache.remove(key);
	}
	
	private Ehcache getCache(String cacheName) {
		try {
			CacheManager manager = CacheManager.create();

			if (!manager.cacheExists(cacheName)) {
				manager.addCache(cacheName);
			}

			return manager.getEhcache(cacheName);
		} catch (Exception e) {
			System.err.println("getCache_" + cacheName + " failed!");
		}
		
		return null;
	}
}