package ru.mjs.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import ru.mjs.dao.IUserAccountDao;
import ru.mjs.dao.cache.BattlecityOnlineCacheManager;
import ru.mjs.model.client.enums.CacheCodeEnum;
import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.database.UserAccountDO;

/**
 * @author procki-u-a * * 
 */
@Repository
public class UserAccountDaoImpl implements IUserAccountDao {
    @Autowired
    private BattlecityOnlineCacheManager cacheManager;
    @Autowired
    private HibernateTemplate hibernateTemplate;
    
    
    @Override
    public void save(UserAccountDO userAccount) throws BattlecityOnlineBaseException {
        hibernateTemplate.save(userAccount);
    }
    
    @Override
    public void update(UserAccountDO userAccount) throws BattlecityOnlineBaseException {
        hibernateTemplate.update(userAccount);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public UserAccountDO findUserAccount(String login, String password) throws BattlecityOnlineBaseException {
        UserAccountDO result = cacheManager.getElement(CacheCodeEnum.USER_ACCOUNT_CACHE.getName(), login);
        
        if(null == result) {
            List<UserAccountDO> references = hibernateTemplate.find("select ua from UserAccountDO ua where ua.login=? and ua.password=?", login, password);
            
            if(CollectionUtils.isEmpty(references)) throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0017);
            
            UserAccountDO reference = references.get(0);
            hibernateTemplate.evict(reference);
            
            cacheManager.addElement(CacheCodeEnum.USER_ACCOUNT_CACHE.getName(), login, reference);
            return reference;
        } else {
            return result;
        }
    }
}
