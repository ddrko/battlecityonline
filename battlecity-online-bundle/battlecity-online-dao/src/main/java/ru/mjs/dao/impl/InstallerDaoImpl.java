package ru.mjs.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import ru.mjs.dao.IInstallerDao;

/**
 * @author procki-u-a * * 
 */
@Repository
public class InstallerDaoImpl implements IInstallerDao {
    @Autowired
    private HibernateTemplate hibernateTemplate;
    
    
    @Override
    public void saveOrUpdate(Object object) {
        hibernateTemplate.saveOrUpdate(object);
    }
    
    @Override
    public void executeSqlQuery(String query) {
        Query sqlQuery = hibernateTemplate.getSessionFactory().getCurrentSession().createSQLQuery(query);
        sqlQuery.executeUpdate();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Object getEntityByNaturalKeys(String query) {
        List<Object> list = hibernateTemplate.find(query);
        
        if(list.size() > 1) throw new RuntimeException("Found many one instance");
        
        if(CollectionUtils.isEmpty(list)) return null;
        
        Object result = list.get(0);
        hibernateTemplate.evict(result);
        return result;
    }
}
