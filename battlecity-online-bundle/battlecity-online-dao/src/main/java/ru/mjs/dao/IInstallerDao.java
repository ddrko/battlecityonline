package ru.mjs.dao;

/**
 * @author procki-u-a * * 
 */
public interface IInstallerDao {
    public void saveOrUpdate(Object object);
    public void executeSqlQuery(String query);
    public Object getEntityByNaturalKeys(String query);
}
