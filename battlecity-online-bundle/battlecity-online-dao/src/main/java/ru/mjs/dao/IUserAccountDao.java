package ru.mjs.dao;

import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.database.UserAccountDO;

/**
 * @author procki-u-a * * 
 */
public interface IUserAccountDao {
    public void save(UserAccountDO userAccount) throws BattlecityOnlineBaseException;
    public void update(UserAccountDO userAccount) throws BattlecityOnlineBaseException;
    public UserAccountDO findUserAccount(String login, String password) throws BattlecityOnlineBaseException;
}
