package ru.mjs.dao.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

import ru.mjs.dao.IUserRoleDao;
import ru.mjs.dao.cache.BattlecityOnlineCacheManager;
import ru.mjs.model.client.enums.CacheCodeEnum;
import ru.mjs.model.client.enums.ReturnCodeEnum;
import ru.mjs.model.client.exception.BattlecityOnlineBaseException;
import ru.mjs.model.database.UserAccountRoleDO;

/**
 * @author procki-u-a * * 
 */
@Repository
public class UserRoleDaoImpl implements IUserRoleDao {
    @Autowired
    private BattlecityOnlineCacheManager cacheManager;
    @Autowired
    private HibernateTemplate hibernateTemplate;
    
    
    @SuppressWarnings("unchecked")
    @Override
    public List<UserAccountRoleDO> getAllUserRoles() throws BattlecityOnlineBaseException {
        List<UserAccountRoleDO> result = hibernateTemplate.find("select ur from UserAccountRoleDO ur");
        
        if(CollectionUtils.isEmpty(result)) {
            throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0015);
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public UserAccountRoleDO findUserRoleByName(String userRoleName) throws BattlecityOnlineBaseException {
        UserAccountRoleDO result = cacheManager.getElement(CacheCodeEnum.USER_ROLE_CACHE.getName(), userRoleName);
        
        if(null == result) {
            List<UserAccountRoleDO> references = hibernateTemplate.find("select ur from UserAccountRoleDO ur where ur.name=?", userRoleName);
            
            if(CollectionUtils.isEmpty(references)) throw new BattlecityOnlineBaseException(ReturnCodeEnum.RC_0015);
            
            UserAccountRoleDO reference = references.get(0);
            hibernateTemplate.evict(reference);
            
            cacheManager.addElement(CacheCodeEnum.USER_ROLE_CACHE.getName(), userRoleName, reference);
            return reference;
        } else {
            return result;
        }
    }
}