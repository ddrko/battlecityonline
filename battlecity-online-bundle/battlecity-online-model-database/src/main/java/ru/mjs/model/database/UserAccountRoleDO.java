package ru.mjs.model.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author procki-u-a * * 
 */
@Data
@Entity
@Table(name = "UserAccountRole")
public class UserAccountRoleDO implements Serializable {
    private static final long serialVersionUID = -2496659634847976046L;
    
    @Id
    @Column(name = "UserAccountRoleID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "Name")
    private String name;
}
