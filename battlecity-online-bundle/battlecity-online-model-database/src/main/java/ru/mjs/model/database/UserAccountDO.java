package ru.mjs.model.database;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.Data;

/**
 * @author procki-u-a * * 
 */
@Data
@Entity
@Table(name = "UserAccount")
public class UserAccountDO implements Serializable {
    private static final long serialVersionUID = 5093047193845934641L;
    
    @Id
    @Column(name = "UserAccountID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @ManyToOne
    @Cascade(CascadeType.DETACH)
    @JoinColumn(name = "UserAccountRoleID")
    private UserAccountRoleDO userAccountRole;
    
    @Column(name = "Login")
    private String login;
    
    @Column(name = "Password")
    private String password;
    
    @Column(name = "Nickname")
    private String nickname;
    
    @Column(name = "Disabled")
    private Boolean disabled;
    
    @Column(name = "RegistrationDate")
    private Date registrationDate;
    
    @Column(name = "LastSignInDate")
    private Date lastSignInDate;
}